<?php
    abstract class computadora{
        abstract protected function limpieza();
    }

    class procesador extends computadora{
        public function limpieza(){
            echo "Limpieza hecha al procesador correctamente!";
        }
    }

    $obj = new procesador();
    $obj->limpieza();
?>