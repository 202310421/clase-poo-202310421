<?php
    class sobrecarga{
        public static function metodo1(){

            $argumentos = func_get_args();
            $numeroArgs = count($argumentos);
    
            if ($numeroArgs == 0) echo 'Hola mundo';
            else if ($numeroArgs == 1) echo 'Hola mundo', $argumentos[0];
        }
        public function metodo2(){
            $num1 = func_num_args();

            if($num1 <=500){
                $num1 = $num1 + 1;
                echo "<br>se sumó uno a la variable";
            }else{
                $num1 = 0;
                echo "EL valor se reseteo";
            }
        }
       
    }

    $obj = new sobrecarga();
    $obj->metodo1();
    $obj->metodo2();
?>