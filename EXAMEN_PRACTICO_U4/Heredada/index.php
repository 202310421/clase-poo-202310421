<?php
    interface Aire_Acondicionado{
        public function prender();
        public function bajar_temperatura();
    }

    class cuarto implements Aire_Acondicionado {
        public function prender(){
            echo "Aire prendido correctamente!<br>";
        }
        public function bajar_temperatura()
        {
            echo "Temperatura del aire ha bajado a 18C°";
        }
    }

    $obj = new cuarto();
    $obj->prender();
    $obj->bajar_temperatura();
?>