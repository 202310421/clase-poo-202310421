<?php
    class pc{
        public function __construct($mhz)
        {
            $this->__construct = $mhz;
        }

        public function mostrarmhz($mhz){
            echo "Estos son tus MHZ del cpu: ".$mhz."MHz<br>";
            $this->randommhz($mhz);
        }

        private function randommhz($mhz){
            $mhz = rand(24000, 32000);
            echo "Estos Mhz randomizados: ".$mhz."Mhz<br>";
        }
        protected function cambiarMhz($mhz){
            $mhz = 32000;
            echo "Estos son tus Mhz cambiados: ".$mhz."Mhz<br>";
        }
        public function __destruct()
        {
            echo "<br>En 5 segundos esta página se reiniciará...";
            echo '<meta http-equiv="refresh" content="4; url=index.php">';
        }
    }
    class componentes extends pc{
        
        public function __construct($mhz)
        {
            $this->__construct = $mhz;
        }

        public function showmostrarMhz($mhz){
            $this->mostrarmhz($mhz);
            $this->cambiarMhz($mhz);
        }

        public function __destruct1()
        {
            echo "<br>Los datos de esta página se reiniciarán.";
        }
    }
    $mhz = 32000;
    $obj = new componentes($mhz);
    $obj->showmostrarMhz($mhz);
    $obj->__destruct1();
?>