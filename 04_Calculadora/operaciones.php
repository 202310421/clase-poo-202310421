<?php

    class operaciones{
        public $operacionRealizar = "";

        public function suma($a, $b){
            return $a + $b;
        }
        public function division($a, $b){
            return $a / $b;
        }
        public function resta($a, $b){
            return $a - $b;
        }
        public function multiplicacion($a, $b){
            return $a * $b;
        }

        Public function ResultadoOperacion($a, $b){

            switch ($this->operacionRealizar){
                case 'suma':
                    return $this->suma($a, $b);
                    break;
                case 'resta':
                    return $this->resta($a, $b);
                    break;
                case 'division':
                    return $this->division($a, $b);
                    break;
                case 'multiplicacion':
                    return $this->multiplicacion($a, $b);
                    break;
                default:
                return 'Operación a realizar no definida';
                break;

            }
        }
    }

?>