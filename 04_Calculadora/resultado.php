<?php

    include 'operaciones.php';

    if (isset($_POST['operador'])){

        $operador = $_POST['operador'];
        $valor1 = $_POST['valor1'];
        $valor2 = $_POST['valor2'];

        $obj = new operaciones();

        $obj->operacionRealizar = $operador;
        $RESULTADO = $obj->ResultadoOperacion($valor1, $valor2);

        echo "El resultado de la operación ".$operador." es: ".$RESULTADO;

        echo '<meta http-equiv="refresh" content="4; url=../index.html">';
    }

?>