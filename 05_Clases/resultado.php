<?php
    //Incluimos nuestro código "Operaciones.php"
    include 'operaciones.php';

    if (isset($_POST['operador'])){
        //Obtenemos los resultados de nuestra página a nuestro código
        $operador = $_POST['operador'];
        $temp = $_POST['temp'];  
        $obj = new operaciones();
        //usamos el metodo de nuestro código
        $obj->operacionRealizar = $operador;
        //usamos la función que nos regresará el resultado de nuestro código
        $RESULTADO = $obj->ResultadoOperacion($temp);
        //Mostramos el código
        echo "El resultado de la operación ".$operador." es: ".$RESULTADO;

        echo '<meta http-equiv="refresh" content="4; url=../index.html">';
    }

?>