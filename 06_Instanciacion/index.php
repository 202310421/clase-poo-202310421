<?php
    class cliente{ 
        var $nombre; 
        var $numero; 
 
        function __construct($nombre,$numero){ 
           $this->nombre=$nombre; 
           $this->numero=$numero;       
        } 
 
        function numero(){ 
           return $this->numero; 
        } 
 }
    $cliente1 = new cliente("Pepe", 1); 
    $cliente2 = new cliente("Roberto", 564); 


    echo "El identificador del cliente ".$cliente1->nombre." es: " . $cliente1->numero(); 
    echo "<br>El identificador del cliente ".$cliente2->nombre." es: " . $cliente2->numero();
    
?>