<?php
    class memoria_usb{
       public $color = "";
       public $almacenamiento = 0;
       public $marca = "";
       
       public function setAlmacenamiento($mb_memoria){
           $this->almacenamiento = $mb_memoria;
       }

       public function getAlmacenamiento(){
           return $this->almacenamiento."MB";
       }
       
       public function setColor($color){
           $this->color = $color;
       }

       public function getColor(){
           return $this->color;
       }
       
       public function setMarca($marca){
           $this->marca = $marca;
       }

       public function getMarca(){
           return $this->marca;
       }

    }

    $objKingston = new memoria_usb();

    $objKingston->setAlmacenamiento(16000);
    $objKingston->setColor("blanco");
    $objKingston->setMarca("Kingston");

    echo "Almacenamiento de memoria: ". $objKingston->getAlmacenamiento(). "<br>";
    echo "Color de Memoria: ". $objKingston->getColor(). "<br>";
    echo "Marca de Memoria: ". $objKingston->getMarca(). "<br>";
?>